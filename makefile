CC=tcc
FLAGS=-Wall -Werror -pedantic -O3
LIBS=-Ilib

all: obj test main clean

obj:
	mkdir -p obj

bin:
	mkdir -p bin

main: obj bin
	@echo "----- Main compilation ------" 
	${CC} --version
	${CC} -o obj/main.o -c src/main.c ${FLAGS} ${LIBS} 
	${CC} -o ./bin/sitl obj/main.o ${FLAGS} ${LIBS}
	echo -n "Hello World!" > toto.sitl
	./bin/sitl --file=toto.sitl

test: string.test bin
	@echo "----- String test running ------" 
	./bin/string.test

string.test: obj bin
	@echo "----- String test compilation ------" 
	${CC} -o obj/string.test.o -c test/string.test.c ${FLAGS} ${LIBS} 
	${CC} -o bin/string.test obj/string.test.o ${FLAGS} ${LIBS}

clean: 
	rm -rf obj bin *.s *.ii *.i 
