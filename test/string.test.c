#include <lib/string.h>
#include <lib/literal.h>
#include <lib/test.h>
#include <stdio.h>

inline static boolean if_c(const void *v)
{
  return *(const char *)v == 'c';
}

inline static void test_range_count_if()
{
  string s;
  string_build_from_literal(&s, "coucou");
  test_true(range_count_if((range *)&s, if_c, sizeof(char)) == 2);
  string_finalize(&s);
}

inline static void test_range_find_if()
{
  string s;
  string_build_from_literal(&s, "coucou");
  char *found = range_find_if((range *)&s, if_c, sizeof(char));
  test_true(found != NULL && *found == 'c');
  string_finalize(&s);
}

inline static void test_range_equals()
{
  string s, s2;
  string_build_from_literal(&s, "coucou");
  string_build_from_literal(&s2, "coucou");
  test_true(string_equals(&s, &s2));
  string_finalize(&s);
  string_finalize(&s2);
}


inline static void test_range_startswith()
{
  string s, s2, s3;
  string_build_from_literal(&s, "coucou");
  string_build_from_literal(&s2, "cou");
  string_build_from_literal(&s3, "ouc");
  test_true(string_startswith(&s, &s2));
  test_false(string_startswith(&s, &s3));
  string_finalize(&s);
  string_finalize(&s2);
  string_finalize(&s3);
}


inline static void test_range_anyof()
{
  string s;
  string_build_from_literal(&s, "coucou");
  test_true(range_anyof((range *)&s, if_c, sizeof(char)));
  string_finalize(&s);
}

inline static void test_range_allof()
{
  string s;
  string_build_from_literal(&s, "ccccccc");
  test_true(range_anyof((range *)&s, if_c, sizeof(char)));
  string_finalize(&s);
  string s2;
  string_build_from_literal(&s2, "oooooooo");
  test_false(range_anyof((range *)&s2, if_c, sizeof(char)));
  string_finalize(&s2);
}

inline static void test_range_noneof()
{
  string s;
  string_build_from_literal(&s, "ooooocoo");
  test_false(range_noneof((range *)&s, if_c, sizeof(char)));
  string_finalize(&s);
  string s2;
  string_build_from_literal(&s2, "oooooooo");
  test_true(range_noneof((range *)&s2, if_c, sizeof(char)));
  string_finalize(&s2);
}

int main()
{
  test_suite(test_range_count_if);
  test_suite(test_range_find_if);
  test_suite(test_range_equals);
  test_suite(test_range_startswith);
  test_suite(test_range_anyof);
  test_suite(test_range_allof);
  test_suite(test_range_noneof);
  return 0;
}
