#ifndef __lib_utility_h__
#define __lib_utility_h__

typedef unsigned int uint;
typedef unsigned long ulong;

typedef int boolean;

#define true 1
#define false 0

#ifndef NULL
#define NULL 0
#endif

#endif
