
#ifndef __lib_test_h__
#define __lib_test_h__

#include <lib/utility.h>
#include <stdio.h>

#ifndef test_true
#define test_true(expr)                \
  fprintf(stdout, "test " #expr " %s", \
          (expr)                       \
              ? "\033[32mOK\033[0m\n"  \
              : " \033[31mKO\033[0m\n");
#endif

#ifndef test_false
#define test_false(expr)                                    \
  fprintf(stdout, "test \033[1m\033[32mnot\033[0m " #expr " %s", \
          (!expr)                                           \
              ? "\033[32mOK\033[0m\n"                       \
              : "\033[31mKO\033[0m\n");
#endif

#ifndef test_suite
#define test_suite(suite)                                     \
  fprintf(stdout, "run suite of \033[1m" #suite "\033[0m\n"); \
  suite();
#endif

#endif
