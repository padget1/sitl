#ifndef __lib_ios_h__
#define __lib_ios_h__ 

#include <stdio.h>
#include <lib/utility.h>

static inline uint ios_fsize(FILE* f) 
{
  fseek(f, 0, SEEK_END);
  uint size = ftell(f);
  rewind(f);
  return size;
} 


#endif
