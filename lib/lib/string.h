#ifndef __lib_string_h__
#define __lib_string_h__

#include <lib/range.h>
#include <lib/utility.h>
#include <stdlib.h>

typedef char string_storage_type;
typedef char *string_iterator;
typedef const char *string_citerator;

#define STRING_STORAGE_SIZE sizeof(string_storage_type)

struct string
{
  range data;
  uint maxi;
};

typedef struct string string;

inline static void __string_increase(string *s, uint nbadd)
{
  s->maxi += nbadd;
  s->data.b = realloc(s->data.b, STRING_STORAGE_SIZE * s->maxi);
}

inline static void string_clear(string *s)
{
  s->data.lgth = 0;
}

inline static void string_init_default(string *s)
{
  range_build(&s->data, NULL, 0);
  s->maxi = 0;
}

inline static void string_finalize(string *s)
{
  if (s->data.b != NULL)
  {
    free(s->data.b);
    string_init_default(s);
  }
}

inline static string_iterator string_begin(string *s)
{
  return (string_iterator)range_begin((range *)s);
}

inline static string_iterator string_end(string *s)
{
  return (string_iterator)range_end((range *)s, STRING_STORAGE_SIZE);
}

inline static string_citerator string_cbegin(const string *s)
{
  return (string_iterator)range_cbegin((const range *)s);
}

inline static string_citerator string_cend(const string *s)
{
  return (string_iterator)range_cend((const range *)s, STRING_STORAGE_SIZE);
}

inline static void string_pushc(string *s, char c)
{
  if (s->data.lgth == s->maxi)
    __string_increase(s, s->maxi + s->maxi / 2 + 10);

  *string_end(s) = c;
  s->data.lgth += 1;
}

inline static void string_pushs(string *s, const string *s2)
{
  string_citerator b = string_cbegin(s2);
  string_citerator e = string_cend(s2);

  while (b != e)
  {
    string_pushc(s, *b);
    ++b;
  }
}

inline static void string_build(string *s, uint maxi)
{
  char *data = malloc(STRING_STORAGE_SIZE * maxi);
  range_build(&s->data, data, 0);
  s->maxi = maxi;
}

inline static void string_build_from_literal(string *s, const char *literal)
{
  string_init_default(s);

  for (const char *i = literal; *i != 0; ++i)
    string_pushc(s, *i);
}

inline static void string_copy_to(string *s, const string *s2)
{
  if (s != s2)
  {
    string_clear(s);

    if (s->maxi < s2->data.lgth)
      __string_increase(s, s2->data.lgth - s->maxi);

    string_pushs(s, s2);
  }
}

inline static void string_move_to(string *s, string *s2)
{
  if (s != s2)
  {
    string_finalize(s);
    *s = *s2;
    string_init_default(s2);
  }
}

inline static boolean __string_char_eqls(
    const void *c, const void *c2)
{
  return *(string_citerator)c == *(string_citerator)c2;
}

inline static boolean string_equals(
    const string *s,
    const string *s2)
{
  return range_equals(
      (const range *)s,
      (const range *)s2,
      __string_char_eqls,
      STRING_STORAGE_SIZE);
}

inline static boolean string_startswith(
    const string *s,
    const string *s2)
{
  return range_startswith(
      (const range *)s,
      (const range *)s2,
      __string_char_eqls,
      STRING_STORAGE_SIZE);
}

typedef int wstring_storage_type;
typedef wstring_storage_type *wstring_iterator;
typedef const wstring_storage_type *wstring_citerator;

#define WSTRING_STORAGE_SIZE sizeof(wstring_storage_type)

struct wstring
{
  range data;
  uint maxi;
};

typedef struct wstring wstring;

inline static void __wstring_increase(wstring *s, uint nbadd)
{
  s->data.b = realloc(s->data.b, WSTRING_STORAGE_SIZE * (s->maxi + nbadd));
  s->maxi += nbadd;
}

inline static void wstring_clear(wstring *s)
{
  s->data.lgth = 0;
}

inline static void wstring_init_default(wstring *s)
{
  range_build(&s->data, NULL, 0);
  s->maxi = 0;
}

inline static void wstring_finalize(wstring *s)
{
  if (s->data.b != NULL)
  {
    free(s->data.b);
    wstring_init_default(s);
  }
}

inline static wstring_iterator wstring_begin(wstring *s)
{
  return (wstring_iterator)range_begin(&s->data);
}

inline static wstring_iterator wstring_end(wstring *s)
{
  return (wstring_iterator)range_end(&s->data, WSTRING_STORAGE_SIZE);
}

inline static wstring_citerator wstring_cbegin(const wstring *s)
{
  return (wstring_iterator)range_cbegin(&s->data);
}

inline static wstring_citerator wstring_cend(const wstring *s)
{
  return (wstring_iterator)range_cend(&s->data, WSTRING_STORAGE_SIZE);
}

inline static void wstring_pushc(wstring *s, int c)
{
  if (s->data.lgth == s->maxi)
    __wstring_increase(s, s->maxi + s->maxi / 2 + 10);

  *wstring_end(s) = c;
  s->data.lgth += 1;
}

inline static void wstring_pushs(wstring *s, const wstring *s2)
{
  wstring_citerator b = wstring_cbegin(s2);
  wstring_citerator e = wstring_cend(s2);

  while (b != e)
  {
    wstring_pushc(s, *b);
    ++b;
  }
}

inline static void wstring_build(wstring *s, uint maxi)
{
  int *data = malloc(WSTRING_STORAGE_SIZE * maxi);
  range_build(&s->data, data, 0);
  s->maxi = maxi;
}

inline static void wstring_build_from_literal(wstring *s, const char *literal)
{
  wstring_init_default(s);

  for (const char *i = literal; *i != 0; ++i)
    wstring_pushc(s, (int)*i);
}

inline static void wstring_copy_to(wstring *s, const wstring *s2)
{
  if (s != s2)
  {
    wstring_clear(s);

    if (s->maxi < s2->data.lgth)
      __wstring_increase(s, s2->data.lgth - s->maxi);

    wstring_pushs(s, s2);
  }
}

inline static void wstring_move_to(wstring *s, wstring *s2)
{
  if (s != s2)
  {
    wstring_finalize(s);
    *s = *s2;
    wstring_init_default(s2);
  }
}

inline static boolean __wstring_wchar_eqls(
    const void *c, const void *c2)
{
  return *(wstring_citerator)c == *(wstring_citerator)c2;
}

inline static boolean wstring_equals(
    const wstring *s,
    const wstring *s2)
{
  return range_equals(
      (const range *)s,
      (const range *)s2,
      __wstring_wchar_eqls,
      WSTRING_STORAGE_SIZE);
}

inline static boolean wstring_startswith(
    const wstring *s,
    const wstring *s2)
{
  return range_startswith(
      (const range *)s,
      (const range *)s2,
      __wstring_wchar_eqls,
      WSTRING_STORAGE_SIZE);
}

#endif
