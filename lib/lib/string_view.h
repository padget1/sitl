#ifndef __lib_sv_h__
#define __lib_sv_h__

#include <lib/range.h>

typedef range stringv;
typedef const char *stringv_iterator;
typedef const char *stringv_citerator;

inline static void stringv_build(
    stringv *sv,
    const char *b,
    const char *e)
{
  range_build(sv, (void*) b, (void*) e - b);
}

inline static void stringv_copy_to(
    stringv *s, const stringv *s2)
{
  range_copy_to(s, s2);
}

inline static void stringv_move_to(
    stringv *s, stringv *s2)
{
  range_move_to(s, s2);
}

inline static stringv_iterator
stringv_begin(stringv *s)
{
  return (stringv_iterator)range_begin(s);
}

inline static stringv_iterator
stringv_end(stringv *s)
{
  return (stringv_iterator)range_end(s, sizeof(char));
}

inline static stringv_citerator
stringv_cbegin(const stringv *s)
{
  return (stringv_iterator)range_cbegin(s);
}

inline static stringv_citerator
stringv_cend(const stringv *s)
{
  return (stringv_iterator)range_cend(s, sizeof(char));
}

typedef range wstringv;
typedef const int *wstringv_iterator;
typedef const int *wstringv_citerator;

inline static void wstringv_build(
    wstringv *sv,
    const int *b,
    const int *e)
{
  range_build(sv, b, e - b);
}

inline static void wstringv_copy_to(wstringv *s, const wstringv *s2)
{
  range_copy_to(s, s2);
}

inline static void wstringv_move_to(wstringv *s, wstringv *s2)
{
  range_move_to(s, s2);
}

inline static wstringv_iterator wstringv_begin(wstringv *s)
{
  return (wstringv_iterator)range_begin(s);
}

inline static wstringv_iterator wstringv_end(wstringv *s)
{
  return (wstringv_iterator)range_end(s, sizeof(int));
}

inline static wstringv_citerator wstringv_cbegin(const wstringv *s)
{
  return (wstringv_iterator)range_cbegin(s);
}

inline static wstringv_citerator wstringv_cend(const wstringv *s)
{
  return (wstringv_iterator)range_cend(s, sizeof(int));
}

#endif
