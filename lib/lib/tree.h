#ifndef __lib_tree_h__
#define __lib_tree_h__

#include <lib/range.h>

struct tree
{
  range data;
};

struct tree_node
{
  uint child;
  uint next;
  void* value;
};

typedef struct tree tree;
typedef struct tree_node tree_node;

#endif
