#ifndef __lib_args_h__
#define __lib_args_h__

#include <lib/utility.h>
#include <lib/literal.h>

inline static boolean __args_is_equals(const void* restrict c) 
{
  return *(const char*) c=='=';
} 

inline static boolean __args_char_eqls(
    const void * restrict c, const void * restrict c2)
{
  return *(const char*)c == *(const char*)c2;
}

inline static void args_find(
  const literal * restrict argname, 
  literal * restrict value, 
  char **argv, int argc)
{
  for(int i=0; i<argc; ++i) 
  {
    literal arg;
    literal_build(&arg, argv[i]);

    if (range_startswith(&arg, argname, __args_char_eqls, sizeof(char)))
    {
      literal_citerator bv = range_cfind_if(&arg, __args_is_equals, sizeof(char));
      literal_build(value, bv+1);
    } 
  } 
} 


#endif
