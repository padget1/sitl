#ifndef __lib_literal_h__
#define __lib_literal_h__

#include <lib/range.h>

typedef range literal;
typedef const char *literal_citerator;

inline static uint __literal_length(
    const char * restrict l)
{
  const char *b = l;

  while (*l != '\0')
    ++l;

  return l - b;
}

inline static void literal_build(
    literal * restrict l,
    const char * restrict s)
{
  range_build(l, (void *)s, __literal_length(s));
}

inline static void literal_copy_to(
literal * restrict l, const literal *restrict l2)
{
  range_copy_to(l, l2);
}

inline static void literal_move_to(
    literal * restrict l, literal * restrict l2)
{
  range_move_to(l, l2);
}

inline static literal_citerator
literal_begin(literal * restrict l)
{
  return (literal_citerator) range_begin(l);
}

inline static literal_citerator
literal_end(literal * restrict l)
{
  return (literal_citerator) range_end(l, sizeof(const char));
}

inline static literal_citerator
literal_cbegin(const literal * restrict l)
{
  return (literal_citerator) range_cbegin(l);
}

inline static literal_citerator
literal_cend(const literal * restrict l)
{
  return (literal_citerator) range_cend(l, sizeof(const char));
}

 
#endif
