#ifndef __lib_range_h__
#define __lib_range_h__

#include <lib/utility.h>

struct range
{
  uint lgth;
  void *b;
};

typedef struct range range;

inline static void range_build(
    range * restrict r, void * restrict b,
    uint lgth)
{
  r->lgth = lgth;
  r->b = b;
}

inline static void range_copy_to(
    range * restrict r, const range * restrict r2)
{
  *r = *r2;
}

inline static void range_move_to(
    range *restrict r, range * restrict r2)
{
  range_copy_to(r, r2);
  *r2 = (range){0, NULL};
}

inline static char *__aschar(void * restrict v)
{
  return (char *)v;
}

inline static void *range_begin(range * restrict r)
{
  return r->b;
}

inline static void *range_end(
    range *restrict r, const uint tsize)
{
  return __aschar(r->b) + tsize * r->lgth;
}

inline static const void *range_cbegin(const range *restrict r)
{
  return r->b;
}

inline static const void *range_cend(
    const range * restrict r, const uint tsize)
{
  return __aschar(r->b) + tsize * r->lgth;
}

inline static void *range_next(
    void * restrict p, uint tsize)
{
  return ((char *)p) + tsize;
}

inline static const void *range_cnext(
    const void * restrict p, const uint tsize)
{
  return ((const char *)p) + tsize;
}

inline static uint range_count_if(
    const range * restrict r,
    boolean (*pred)(const void *),
    const uint tsize)
{
  uint cnt = 0;

  const void *b = range_cbegin(r);
  const void *e = range_cend(r, tsize);

  while (b != e)
  {
    if (pred(b))
      ++cnt;

    b = range_cnext(b, tsize);
  }

  return cnt;
}

inline static void *range_find_if(
    range * restrict r,
    boolean (*pred)(const void *),
    const uint tsize)
{
  void *b = range_begin(r);
  void *e = range_end(r, tsize);

  while (b != e && !pred(b))
    b = range_next(b, tsize);

  return b != e ? b : NULL;
}

inline static const void *range_cfind_if(
    const range * restrict r,
    boolean (*pred)(const void *),
    const uint tsize)
{
  const void *b = range_cbegin(r);
  const void *e = range_cend(r, tsize);

  while (b != e && !pred(b))
    b = range_cnext(b, tsize);

  return b != e ? b : NULL;
}

inline static void *range_find_if_not(
    range * restrict r,
    boolean (*pred)(const void *),
    const uint tsize)
{
  void *b = range_begin(r);
  void *e = range_end(r, tsize);

  while (b != e && pred(b))
    b = range_next(b, tsize);

  return b != e ? b : NULL;
}

inline static const void *range_cfind_if_not(
    const range * restrict r,
    boolean (*pred)(const void *),
    const uint tsize)
{
  const void *b = range_cbegin(r);
  const void *e = range_cend(r, tsize);

  while (b != e && pred(b))
    b = range_cnext(b, tsize);

  return b != e ? b : NULL;
}

inline static boolean range_allof(
    const range *restrict r,
    boolean (*pred)(const void *),
    const uint tsize)
{
  return range_cfind_if_not(r, pred, tsize) == NULL;
}

inline static boolean range_noneof(
    const range *restrict r,
    boolean (*pred)(const void *),
    const uint tsize)
{
  return range_cfind_if(r, pred, tsize) == NULL;
}

inline static boolean range_anyof(
    const range * restrict r,
    boolean (*pred)(const void *),
    const uint tsize)
{
  return range_cfind_if(r, pred, tsize) != NULL;
}

inline static boolean range_equals(
    const range *restrict r,
    const range *restrict r2, 
    boolean (*eqls)(const void *, const void *),
    const uint tsize)
{
  const void *b = range_cbegin(r);
  const void *e = range_cend(r, tsize);

  const void *b2 = range_cbegin(r2);
  const void *e2 = range_cend(r2, tsize);

  while (b != e && b2 != e2 && eqls(b, b2)) 
  {
    b = range_cnext(b, tsize);
    b2 = range_cnext(b2, tsize);
  } 

  return b == e;
}

inline static boolean range_startswith(
    const range *restrict r,
    const range *restrict r2,
    boolean (*eqls)(const void *, const void *),
    const uint tsize)
{
  const void *b = range_cbegin(r);
  const void *e = range_cend(r, tsize);

  const void *b2 = range_cbegin(r2);
  const void *e2 = range_cend(r2, tsize);

  while (b != e && b2 != e2 && eqls(b, b2)) 
  {
    b = range_cnext(b, tsize);
    b2 = range_cnext(b2, tsize);
  } 

  return b2 == e2;
}


#endif
