#include <stdio.h>
#include <lib/args.h>
#include <lib/literal.h>
#include <lib/string.h>
#include <lib/ios.h>

int main(int argc, char **argv)
{
  literal file, value;
  // string content;

  printf("sitl compiler\n");

  // capture the filename;
  literal_build(&file, "--file");
  literal_build(&value, "");

  args_find(&file, &value, argv, argc);
  
  if (value.lgth != 0) 
    printf("file found in arg line : '%s'\n", literal_begin(&value));
  else 
  {
    printf("file not found\n");
    return 1;
  }

  // open the file to read
  FILE* f = fopen(literal_begin(&value), "r");

  if (f == NULL) 
  {
    printf("file not found");
    return 1;
  } 
  
  // read all file content
  uint size = ios_fsize(f);
  string ctnt;
  string_build(&ctnt, size);
  
  uint res = fread(string_begin(&ctnt), 1, size, f);
  ctnt.data.lgth = res;

  // close the file
  fclose(f);
  
  printf("content of file: %d \n '%*s'\n", res, ctnt.data.lgth, string_begin(&ctnt));
 
  // sitl_tree ast;
  // sitl_parse(&ast, & ctnt);

  // finalize memory
  string_finalize(&ctnt);

  return 0;
}
