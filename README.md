# sitl
Simple Information Technology Language
```c
char (*) cs; // managed pointer
char * cs; // free pointer
```

```c
struct person
{
  string name;
} ;
```
```c
build person(string name) 
{
  _.name = name;
} 
```
```c
copy person(const person& p) 
{
  _.name = p.name;
} 
```
```c
move person(person& p) 
{
  _.name := p.name;
}
```
```c
del person() = default;

```
